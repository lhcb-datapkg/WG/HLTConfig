########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCD0, StandardMCPions

#Truth matched commonparticles: 
_mcd0 = DataOnDemand(Location='Phys/StdMCD02KK/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#
matchDstPiD0 = "(mcMatch('[D*(2010)+ ==> pi+ D0]CC'))"
#matchPions = "(mcMatch('[pi+]cc'))"
#matchD0 = "(mcMatch('[D0]cc'))"

_dst2d0pi = CombineParticles("dst2d0pi")
_dst2d0pi.DecayDescriptor = "[D*(2010)+ -> pi+ D0]cc"
#_dst2d0pi.DaughtersCuts = { "D0" : matchD0, "pi+" : matchPions }
_dst2d0pi.MotherCut = matchDstPiD0
_dst2d0pi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelDst2D0Pi_D02KK = Selection( "SelDst2D0Pi_D02KK",
                        Algorithm = _dst2d0pi,
                        RequiredSelections=[_pions,_mcd0])  

SeqDst2D0Pi_D02KK = SelectionSequence('MCFilter',TopSelection = SelDst2D0Pi_D02KK) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqDst2D0Pi_D02KK],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqDst2D0Pi_D02KK.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqDst2D0Pi_D02KK.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

