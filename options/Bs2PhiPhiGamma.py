########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPhi, StandardMCPhotons, StandardMCKaons

#Truth matched commonparticles:

_phi = DataOnDemand(Location='Phys/StdMCPhi2KK/Particles')
_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')


#
# MC matching
#

#matchphi  = "(mcMatch('phi(1020)'))"
#matchgamma  = "(mcMatch('gamma'))"
matchBs2phiphigamma  = "(mcMatch('[B_s0  ==> phi(1020) phi(1020) gamma]CC'))"

_bs2phiphigamma = CombineParticles("bs2phiphigamma")
_bs2phiphigamma.DecayDescriptor = "B_s0 -> phi(1020) phi(1020) gamma"
_bs2phiphigamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV"}
_bs2phiphigamma.MotherCut = matchBs2phiphigamma
_bs2phiphigamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2phiphigamma = Selection( "SelBs2phiphigamma",
                               Algorithm = _bs2phiphigamma,
                               RequiredSelections=[_phi,_gamma])

SeqBs2phiphigamma = SelectionSequence('MCFilter',TopSelection = SelBs2phiphigamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2phiphigamma],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2phiphigamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2phiphigamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

