########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions, StandardMCPhotons, StandardMCKaons

#Truth matched commonparticles:

_pions = DataOnDemand(Location="Phys/StdMCPions/Particles")
_kaons = DataOnDemand(Location="Phys/StdMCKaons/Particles")
_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')


#
# MC matching
#

#matchphi  = "(mcMatch('phi(1020)'))"
#matchgamma  = "(mcMatch('gamma'))"
matchBd2kpigamma  = "(mcMatch('[B0  ==> K+ pi- gamma]CC'))"

_bd2kpigamma = CombineParticles("bd2kpigamma")
_bd2kpigamma.DecayDescriptor = "[B0 -> K+ pi- gamma]cc"
_bd2kpigamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV"}
_bd2kpigamma.MotherCut = matchBd2kpigamma
_bd2kpigamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2kpiGamma = Selection( "SelBd2kpiGamma",
                            Algorithm = _bd2kpigamma,
                            RequiredSelections=[_pions,_kaons,_gamma])

SeqBd2kpiGamma = SelectionSequence('MCFilter',TopSelection = SelBd2kpiGamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBd2kpiGamma],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2kpiGamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2kpiGamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

