########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKs, StandardMCKaons

#Truth matched commonparticles: 

_mcksll = DataOnDemand(Location='Phys/StdMCKsLL/Particles')
_mcksdd = DataOnDemand(Location='Phys/StdMCKsDD/Particles')
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')


#
# MC matching
#


#matchKaons = "(mcMatch('[K+]cc'))"
#matchKshorts = "(mcMatch('[KS0]cc'))"
matchDsKsK = "(mcMatch('[D_s+  ==> KS0 K+]CC'))"

_ds2ksk = CombineParticles("ds2ksk")
_ds2ksk.DecayDescriptor = "[D_s+ -> KS0 K+]cc"
#_ds2ksk.DaughtersCuts = { "KS0" : matchKshorts, "K+" : matchKaons}
_ds2ksk.MotherCut = matchDsKsK
_ds2ksk.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelDs2KsKLL = Selection( "SelDs2KsKLL",
                        Algorithm = _ds2ksk,
                        RequiredSelections=[_kaons,_mcksll])  

SeqDs2KsKLL = SelectionSequence('SeqDs2KsKLL',TopSelection = SelDs2KsKLL) 

SelDs2KsKDD = Selection( "SelDs2KsKDD",
                        Algorithm = _ds2ksk,
                        RequiredSelections=[_kaons,_mcksdd])  


SeqDs2KsKDD = SelectionSequence('SeqDs2KsKDD',TopSelection = SelDs2KsKDD) 

SeqDs2KsK = MultiSelectionSequence('MCFilter',Sequences = [SeqDs2KsKLL,SeqDs2KsKDD]) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqDs2KsK],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqDs2KsK.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqDs2KsK.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

