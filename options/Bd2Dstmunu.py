########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDstar, StandardMCMuons 

#Truth matched commonparticles: 

#_mcd0 = DataOnDemand(Location='Phys/StdMCD02KPi/Particles')
_mcdst = DataOnDemand(Location='Phys/StdMCDstarWithD02KPi/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#


matchBd2Dstmunu   = "(mcMatch('[[B0]cc ==>  D*(2010)-  mu+ Neutrino ]CC'))"
#matchDst   = "(mcMatch('[D*(2010)-]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc')"

#mothercut
mothercut = "ALL"

_bd2dstmunu = CombineParticles("bd2dstmunu")
_bd2dstmunu.DecayDescriptor = "[B0 -> D*(2010)- mu+]cc"
#_bd2dstmunu.DaughtersCuts = { "D*(2010)-" : matchDst, "mu+" : matchmuons }
_bd2dstmunu.MotherCut = matchBd2Dstmunu
_bd2dstmunu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2Dstmunu = Selection( "SelBd2Dstmunu",
                        Algorithm = _bd2dstmunu,
                        RequiredSelections=[_mcdst,_muons])  

SeqBd2Dstmunu = SelectionSequence('MCFilter',TopSelection = SelBd2Dstmunu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2Dstmunu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2Dstmunu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2Dstmunu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

