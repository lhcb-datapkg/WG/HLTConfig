########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCLambda0, StandardMCPions

#Truth matched commonparticles: 
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')
_lambdas = DataOnDemand(Location='Phys/StdMCLambda02pPi/Particles')


#
# MC matching
#


#matchLambdas = "(mcMatch('[Lambda0]cc'))"
#matchPions = "(mcMatch('[pi+]cc'))"
matchLcLPi = "(mcMatch('[Lambda_c+ ==> Lambda0 pi+]CC'))"


_lc2lpi = CombineParticles("lc2lpi")
_lc2lpi.DecayDescriptor = "[Lambda_c+ -> Lambda0 pi+]cc"
#_lc2lpi.DaughtersCuts = { "Lambda0" : matchLambdas, "pi+" : matchPions }
_lc2lpi.MotherCut = matchLcLPi
_lc2lpi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelLc2LPi = Selection( "SelLc2LPi",
                        Algorithm = _lc2lpi,
                        RequiredSelections=[_pions,_lambdas])  

SeqLc2LPi = SelectionSequence('MCFilter',TopSelection = SelLc2LPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqLc2LPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqLc2LPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqLc2LPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

