########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCJpsi, StandardMCPhi, StandardMCMuons, StandardMCKaons

#Truth matched commonparticles: 
_psi2s = DataOnDemand(Location='Phys/StdMCpsi2s2MuMu/Particles')
_phi = DataOnDemand(Location='Phys/StdMCPhi2KK/Particles')


#
# MC matching
#

#matchpsi   = "(mcMatch('psi(2S)'))"
#matchphi  = "(mcMatch('phi(1020)'))"
matchBs2psi2sphi  = "(mcMatch('[B_s0 ==> psi(2S)  phi(1020)]CC'))"


_bs2psi2sphi = CombineParticles("bs2psi2sphi")
_bs2psi2sphi.DecayDescriptor = "[B_s0 -> psi(2S)  phi(1020)]cc"
#_bs2psi2sphi.DaughtersCuts = { "psi(2S)" : matchpsi, "phi(1020)" : matchphi }
_bs2psi2sphi.MotherCut = matchBs2psi2sphi
_bs2psi2sphi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2psi2sPhi = Selection( "SelBs2psi2sPhi",
                        Algorithm = _bs2psi2sphi,
                        RequiredSelections=[_psi2s,_phi])  

SeqBs2psi2sPhi = SelectionSequence('MCFilter',TopSelection = SelBs2psi2sPhi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2psi2sPhi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2psi2sPhi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2psi2sPhi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

