########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCMuons, StandardMCProtons
#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')
_protons = DataOnDemand(Location='Phys/StdMCProtons/Particles')


#
# MC matching
#


matchSigma2pmumu   = "(mcMatch('[Sigma+ ==> p+ mu- mu+ ]CC'))"
#matchprotons   = "(mcMatch('[p+]cc'))"
#matchmuons  = "(mcMatch('[mu-]cc'))"

_Sigma2pmumu = CombineParticles("Sigma2pmumu")
_Sigma2pmumu.DecayDescriptor = "[Sigma+ -> p+ mu- mu+]cc"
#_Sigma2pmumu.DaughtersCuts = { "p+" : matchprotons, "mu-" : matchmuons }
_Sigma2pmumu.MotherCut = matchSigma2pmumu
_Sigma2pmumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelSigma2pmumu = Selection( "SelSigma2pmumu",
                        Algorithm = _Sigma2pmumu,
                        RequiredSelections=[_protons,_muons])  

SeqSigma2pmumu = SelectionSequence('MCFilter',TopSelection = SelSigma2pmumu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqSigma2pmumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqSigma2pmumu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqSigma2pmumu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

