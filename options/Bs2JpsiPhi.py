########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPhi, StandardMCJpsi, StandardMCKaons, StandardMCMuons

#Truth matched commonparticles: 
_jpsi = DataOnDemand(Location='Phys/StdMCJpsi2MuMu/Particles')
_phi = DataOnDemand(Location='Phys/StdMCPhi2KK/Particles')


#
# MC matching
#
#matchJpsi   = "(mcMatch('J/psi(1S)'))"
#matchphi  = "(mcMatch('phi(1020)'))"

matchBs2JpsiPhi  = "(mcMatch('[B_s0  ==> J/psi(1S)  phi(1020)]CC'))"


_bs2jpsiphi = CombineParticles("bs2jpsiphi")
_bs2jpsiphi.DecayDescriptor = "[B_s0 -> J/psi(1S)  phi(1020)]cc"
#_bs2jpsiphi.DaughtersCuts = { "J/psi(1S)" : matchJpsi, "phi(1020)" : matchphi }
_bs2jpsiphi.MotherCut = matchBs2JpsiPhi
_bs2jpsiphi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2JpsiPhi = Selection( "SelBs2JpsiPhi",
                        Algorithm = _bs2jpsiphi,
                        RequiredSelections=[_jpsi,_phi])  

SeqBs2JpsiPhi = SelectionSequence('MCFilter',TopSelection = SelBs2JpsiPhi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2JpsiPhi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2JpsiPhi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2JpsiPhi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

