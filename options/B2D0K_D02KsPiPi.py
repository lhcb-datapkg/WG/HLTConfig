########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCD0, StandardMCMuons

#Truth matched commonparticles: 

_mcd0 = DataOnDemand(Location='Phys/StdMCD02KsPiPi/Particles')
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')


#
# MC matching
#

#matchD0   = "(mcMatch('[D0]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"
matchB2D0K  = "(mcMatch('[B+ ==> D~0 K+]CC'))"


_b2d0k = CombineParticles("b2d0k")
_b2d0k.DecayDescriptor = "[B- -> D0 K-]cc"
#_b2d0k.DaughtersCuts = { "D0" : matchD0, "mu+" : matchmuons }
_b2d0k.MotherCut = matchB2D0K
_b2d0k.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelB2D0K_D02KsPiPi = Selection( "SelB2D0K_D02KsPiPi",
                        Algorithm = _b2d0k,
                        RequiredSelections=[_mcd0,_kaons])  

SeqB2D0K_D02KsPiPi = SelectionSequence('MCFilter',TopSelection = SelB2D0K_D02KsPiPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqB2D0K_D02KsPiPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqB2D0K_D02KsPiPi.sequence(),dstWriter.sequence()] )
#DaVinci().UserAlgorithms = [SeqB2D0K_D02KsPiPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

