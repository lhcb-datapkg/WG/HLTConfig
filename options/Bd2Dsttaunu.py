########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDstar, StandardMCTau

#Truth matched commonparticles: 

#_mcd0 = DataOnDemand(Location='Phys/StdMCD02KPi/Particles')
_mcdst = DataOnDemand(Location='Phys/StdMCDstarWithD02KPi/Particles')
_mctaus = DataOnDemand(Location='Phys/StdMCTau/Particles')


#
# MC matching
#


matchBd2Dsttaunu   = "(mcMatch('[[B0]cc ==>  D*(2010)-  tau+ Neutrino ]CC'))"
#matchDst   = "(mcMatch('[D*(2010)-]cc'))"
#matchtau  = "(mcMatch('[tau+]cc'))"


_bd2dsttaunu = CombineParticles("bd2dsttaunu")
_bd2dsttaunu.DecayDescriptor = "[B0 -> D*(2010)- tau+]cc"
#_bd2dsttaunu.DaughtersCuts = { "D*(2010)-" : matchDst, "tau+" : matchtau }
_bd2dsttaunu.MotherCut = matchBd2Dsttaunu
_bd2dsttaunu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2DstTaunu = Selection( "SelBd2DstTaunu",
                        Algorithm = _bd2dsttaunu,
                        RequiredSelections=[_mcdst,_mctaus])  

SeqBd2DstTaunu = SelectionSequence('MCFilter',TopSelection = SelBd2DstTaunu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2DstTaunu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2DstTaunu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2DstTaunu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

