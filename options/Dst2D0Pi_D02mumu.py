########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions, StandardMCMuons

#Truth matched commonparticles: 
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')



#
# MC matching
#
matchD0mumu = "(mcMatch('[D0 ==> mu+ mu-]CC'))"
matchDstPiD0   = "(mcMatch('[D*(2010)+ => D0 pi+]CC'))"


## D0 -> mu+ mu-
_d02mumu = CombineParticles( DecayDescriptor = "[D0 -> mu+ mu-]cc"
                                , MotherCut = matchD0mumu
                                , Preambulo = [
                                        "from LoKiPhysMC.decorators import *",
                                        "from PartProp.Nodes import CC" ]
                              )

_selD02mumu = Selection( "SelD02mumu"
                            , Algorithm = _d02mumu
                            , RequiredSelections = [_pions, _muons] )



## D*+ -> pi+ D0
_dst2D0Pi = CombineParticles( DecayDescriptor = "[D*(2010)+ -> D0 pi+]cc"
                              , MotherCut = matchDstPiD0
                              , Preambulo = [
                                        "from LoKiPhysMC.decorators import *",
                                        "from PartProp.Nodes import CC" ]
                            )

SelDst2D0Pi_D02mumu = Selection( "SelDst2D0Pi_D02mumu"
                                    , Algorithm = _dst2D0Pi
                                    , RequiredSelections = [_pions, _selD02mumu] )

SeqDst2D0Pi_D02mumu = SelectionSequence('MCFilter', TopSelection = SelDst2D0Pi_D02mumu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqDst2D0Pi_D02mumu ],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqDst2D0Pi_D02mumu.sequence(), dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

