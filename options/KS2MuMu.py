########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCMuons, StandardMCProtons
#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')

#
# MC matching
#


matchKs2mumu   = "(mcMatch('KS0 ==> mu- mu+'))"

_Ks2mumu = CombineParticles("Ks2mumu")
_Ks2mumu.DecayDescriptor = "KS0 -> mu- mu+"
#_Ks2mumu.DaughtersCuts = { "p+" : matchprotons, "mu-" : matchmuons }
_Ks2mumu.MotherCut = matchKs2mumu
_Ks2mumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelKs2mumu = Selection( "SelKs2mumu",
                        Algorithm = _Ks2mumu,
                        RequiredSelections=[_muons])  

SeqKs2mumu = SelectionSequence('MCFilter',TopSelection = SelKs2mumu) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqKs2mumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqKs2mumu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqKs2mumu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

