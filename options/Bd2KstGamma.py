########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKstar, StandardMCPhotons, StandardMCKaons

#Truth matched commonparticles:

_kst = DataOnDemand(Location='Phys/StdMCKstar/Particles')
_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')


#
# MC matching
#

#matchphi  = "(mcMatch('phi(1020)'))"
#matchgamma  = "(mcMatch('gamma'))"
matchBd2kstgamma  = "(mcMatch('[B0  ==> K*(892)0 gamma]CC'))"

_bd2kstgamma = CombineParticles("bd2kstgamma")
_bd2kstgamma.DecayDescriptor = "[B0 -> K*(892)0 gamma]cc"
_bd2kstgamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV"}
_bd2kstgamma.MotherCut = matchBd2kstgamma
_bd2kstgamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2KstGamma = Selection( "SelBd2KstGamma",
                        Algorithm = _bd2kstgamma,
                        RequiredSelections=[_kst,_gamma])

SeqBd2KstGamma = SelectionSequence('MCFilter',TopSelection = SelBd2KstGamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBd2KstGamma],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2KstGamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2KstGamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

