########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDsplus, StandardMCPions

#Truth matched commonparticles: 

_mcds = DataOnDemand(Location='Phys/StdMCDsplus2KKPi/Particles')
_mcpions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#

#matchDs   = "(mcMatch('[D_s-]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"
matchBs2DsPi  = "(mcMatch('[[B_s0]cc ==> D_s- pi+]CC'))"


_bs2dspi = CombineParticles("bs2dspi")
_bs2dspi.DecayDescriptor = "[B_s0 -> D_s- pi+]cc"
#_bs2dspi.DaughtersCuts = { "D_s+" : matchDs, "mu-" : matchmuons }
_bs2dspi.MotherCut = matchBs2DsPi
_bs2dspi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2DsPi = Selection( "SelBs2DsPi",
                        Algorithm = _bs2dspi,
                        RequiredSelections=[_mcds,_mcpions])  

SeqBs2DsPi = SelectionSequence('MCFilter',TopSelection = SelBs2DsPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2DsPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2DsPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2DsPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

