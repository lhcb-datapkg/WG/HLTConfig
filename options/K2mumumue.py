########################################################################
####author: Jessica Prisciandaro, jessica.prisciandaro@cern.ch#########
########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCMuons, StandardMCElectrons
#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')
_electrons = DataOnDemand(Location='Phys/StdMCElectrons/Particles')

#
# MC matching
#


matchKs23mue   = "(mcMatch('[KS0 ==> mu- mu+ e+ mu-]CC'))"

_Ks23mue = CombineParticles("Ks23mue")
_Ks23mue.DecayDescriptor = "[KS0 ->  mu- mu+ e+ mu-]cc"

_Ks23mue.MotherCut = matchKs23mue
_Ks23mue.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelKs23mue = Selection( "SelKs23mue",
                        Algorithm = _Ks23mue,
                        RequiredSelections=[_muons, _electrons])  

SeqKs23mue = SelectionSequence('MCFilter',TopSelection = SelKs23mue) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking, fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqKs23mue],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence([SeqKs23mue.sequence(), dstWriter.sequence()])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
