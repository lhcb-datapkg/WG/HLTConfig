########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKs, StandardMCPhotons, StandardMCPions

# Build Kst+

_ks = DataOnDemand(Location='Phys/StdMCKsAny/Particles')
_pions = DataOnDemand(Location="Phys/StdMCPions/Particles")
matchKstp = "(mcMatch('[K*(892)+ ==> KS0 pi+]CC'))"

## create the algorithm
StdMCKstp = CombineParticles("StdMCKstp")
StdMCKstp.DecayDescriptor = "[K*(892)+ -> KS0 pi+]cc"
StdMCKstp.MotherCut = matchKstp
StdMCKstp.Preambulo = [ "from LoKiPhysMC.decorators import *",
                        "from PartProp.Nodes import CC" ]

SelStdMCKstp = Selection ("SelStdMCKstp",
                          Algorithm=StdMCKstp,
                          RequiredSelections=[_pions, _ks])

#Truth matched commonparticles:

_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')


#
# MC matching
#

#matchphi  = "(mcMatch('phi(1020)'))"
#matchgamma  = "(mcMatch('gamma'))"
matchBu2kstgamma  = "(mcMatch('[B+  ==> K*(892)+ gamma]CC'))"

_bu2kstpgamma = CombineParticles("bd2kstgamma")
_bu2kstpgamma.DecayDescriptor = "[B+ -> K*(892)+ gamma]cc"
_bu2kstpgamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV"}
_bu2kstpgamma.MotherCut = matchBu2kstgamma
_bu2kstpgamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBu2KstGamma = Selection( "SelBu2KstGamma",
                            Algorithm = _bu2kstpgamma,
                            RequiredSelections=[SelStdMCKstp,_gamma])

SeqBu2KstGamma = SelectionSequence('MCFilter',TopSelection = SelBu2KstGamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBu2KstGamma],)
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBu2KstGamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBu2KstGamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

