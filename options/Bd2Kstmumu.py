########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKstar, StandardMCMuons

#Truth matched commonparticles: 
_mckst = DataOnDemand(Location='Phys/StdMCKstar/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#



matchBd2Kstmumu   = "(mcMatch('[B0  ==>  K*(892)0  mu+ mu-]CC'))"
#matchmuons   = "(mcMatch('[mu-]cc'))"
#matchkst   = "(mcMatch('[K*(892)0]cc'))"

#mothercut
mothercut = "ALL"

_bd2kstmumu = CombineParticles("bd2kstmumu")
_bd2kstmumu.DecayDescriptor = "[B0 -> K*(892)0  mu+ mu-]cc"
#_bd2kstmumu.DaughtersCuts = { "K*(892)0" : matchkst, "mu+" : matchmuons }
_bd2kstmumu.MotherCut = matchBd2Kstmumu
_bd2kstmumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2Kstmumu = Selection( "SelBd2Kstmumu",
                        Algorithm = _bd2kstmumu,
                        RequiredSelections=[_mckst,_muons])  

SeqBd2Kstmumu = SelectionSequence('MCFilter',TopSelection = SelBd2Kstmumu) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2Kstmumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2Kstmumu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2Kstmumu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

