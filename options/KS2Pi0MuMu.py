########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCMuons, StandardMCPi0
#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')
_respiz = DataOnDemand(Location='Phys/StdMCResolvedPi0/Particles')
_merpiz = DataOnDemand(Location='Phys/StdMCMergedPi0/Particles')

#
# MC matching
#


matchKs2pi0mumu   = "(mcMatch('KS0 ==> pi0 mu- mu+'))"

_Ks2pi0mumu = CombineParticles("Ks2pi0mumu")
_Ks2pi0mumu.DecayDescriptor = "KS0 -> pi0 mu- mu+"

_Ks2pi0mumu.MotherCut = matchKs2pi0mumu
_Ks2pi0mumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelKs2pi0mumu = Selection( "SelKs2pi0mumu",
                        Algorithm = _Ks2pi0mumu,
                        RequiredSelections=[_muons, _respiz, _merpiz])  

SeqKs2pi0mumu = SelectionSequence('MCFilter',TopSelection = SelKs2pi0mumu) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqKs2pi0mumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqKs2pi0mumu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqKs2pi0mumu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

