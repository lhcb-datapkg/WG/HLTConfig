########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDplus

#Truth matched commonparticles: 

#_mcd0 = DataOnDemand(Location='Phys/StdMCD02KPi/Particles')
_mcdplus = DataOnDemand(Location='Phys/StdMCDplus2KPiPi/Particles')


#
# MC matching
#


matchBd2DD  = "(mcMatch('[[B0]cc ==>  D-  D+]CC'))"
#matchDplus   = "(mcMatch('[D+]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"


_bd2dd = CombineParticles("bd2dd")
_bd2dd.DecayDescriptor = "[B0 -> D- D+]cc"
#_bd2dd.DaughtersCuts = { "D-" : matchDplus, "mu+" : matchmuons }
_bd2dd.MotherCut = matchBd2DD
_bd2dd.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2DD = Selection( "SelBd2DD",
                        Algorithm = _bd2dd,
                        RequiredSelections=[_mcdplus])  

SeqBd2DD = SelectionSequence('MCFilter',TopSelection = SelBd2DD) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2DD],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2DD.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2DD.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

