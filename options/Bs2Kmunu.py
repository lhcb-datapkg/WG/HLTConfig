########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCMuons, StandardMCKaons 

#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')


#
# MC matching
#

#matchKaons   = "(mcMatch('[K-]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"
matchBs2Kmunu  = "(mcMatch('[[B_s0]cc ==> K-  mu+ Neutrino ]CC'))"


_bs2kmunu = CombineParticles("bs2kmunu")
_bs2kmunu.DecayDescriptor = "[B_s0 -> K- mu+]cc"
#_bs2kmunu.DaughtersCuts = { "K-" : matchKaons, "mu+" : matchmuons }
_bs2kmunu.MotherCut = matchBs2Kmunu
_bs2kmunu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2Kmunu = Selection( "SelBs2Kmunu",
                        Algorithm = _bs2kmunu,
                        RequiredSelections=[_kaons,_muons])  

SeqBs2Kmunu = SelectionSequence('MCFilter',TopSelection = SelBs2Kmunu) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2Kmunu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2Kmunu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2Kmunu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

