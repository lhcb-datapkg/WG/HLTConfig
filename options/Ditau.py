########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop, SubstitutePID
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, MultiSelectionSequence
from CommonMCParticles import StandardMCDsplus, StandardMCMuons

### '[ H_30 -> pi+ mu- ]cc'
### '[X -> (tau+ -> pi+ ...) (tau- -> mu- ...) ]CC'


#Truth matched commonparticles:
mode = {
  'pi': DataOnDemand(Location='Phys/StdAllNoPIDsPions/Particles'),
  'mu': DataOnDemand(Location='Phys/StdAllNoPIDsMuons/Particles'),
  'e' : DataOnDemand(Location='Phys/StdAllNoPIDsElectrons/Particles'),  # I got bug in StdMCElectrons
  'K' : DataOnDemand(Location='Phys/StdAllNoPIDsKaons/Particles'),
}

import itertools
def gen_comb():
  for c1,c2 in itertools.combinations_with_replacement(mode.keys(), 2):
    sel     = [ mode[c1], mode[c2] ]
    suffix  = '_{c1}_{c2}'.format(**locals())
    decay   = '[ H_30 -> {c1}+ {c2}- ]cc'.format(**locals())
    mcut    = "(mcMatch('[ X -> (tau+ -> {c1}+ ...) (tau- -> {c2}- ...)]CC'))".format(**locals())
    yield suffix, decay, mcut, sel

### MC matching

def gen_sel():
  for suffix, decay, mcut, reqsel in gen_comb():
    comb = CombineParticles('CombDitau'+suffix)
    comb.DecayDescriptor    = decay
    comb.ParticleCombiners  = {'':'MomentumCombiner'}
    comb.MotherCut          = mcut
    comb.Preambulo          = [
        "from LoKiPhysMC.decorators import *",
        "from PartProp.Nodes import CC" ]
    # sel     = Selection( "SelDitau", Algorithm = comb, RequiredSelections=[muons, pions])
    sel     = Selection( "SelDitau"+suffix, Algorithm = comb, RequiredSelections=reqsel)
    selseq  = SelectionSequence('MCFilter'+suffix, TopSelection = sel)
    yield selseq

selseq = MultiSelectionSequence('MCFilter', list(gen_sel()))


#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [selseq],
                          )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
# DaVinci().EvtMax = 1000
DaVinci().EvtMax = -1            # Number of events
DaVinci().appendToMainSequence( [ selseq.sequence(), dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 40


# DaVinci().VerboseMessages = True
