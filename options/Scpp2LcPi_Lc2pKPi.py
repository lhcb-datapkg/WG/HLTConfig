########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCLambdac, StandardMCPions

#Truth matched commonparticles: 
_mcLcp = DataOnDemand(Location='Phys/StdMCLambdac/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#
matchScppLcPi = "(mcMatch('[Sigma_c++ => Lambda_c+ pi+]CC'))"

_scpp2LcPi = CombineParticles( DecayDescriptor = "[Sigma_c++ -> Lambda_c+ pi+]cc"
                              , MotherCut = matchScppLcPi
                              , Preambulo = [
                                        "from LoKiPhysMC.decorators import *",
                                        "from PartProp.Nodes import CC" ]
                            )

SelScpp2LcPi_Lc2pKPi = Selection( "SelScpp2LcPi_Lc2pKPi"
                                  , Algorithm = _scpp2LcPi
                                  , RequiredSelections = [_pions, _mcLcp] )

SeqScpp2LcPi_Lc2pKPi = SelectionSequence('MCFilter', TopSelection = SelScpp2LcPi_Lc2pKPi) 


#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqScpp2LcPi_Lc2pKPi ]
                        )

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqScpp2LcPi_Lc2pKPi.sequence(), dstWriter.sequence() ])


# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

