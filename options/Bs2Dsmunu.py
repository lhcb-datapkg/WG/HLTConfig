########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDsplus, StandardMCMuons

#Truth matched commonparticles: 
_mcds = DataOnDemand(Location='Phys/StdMCDsplus2KKPi/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#

#matchDs   = "(mcMatch('[D_s-]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"
matchBs2Dsmunu  = "(mcMatch('[[B_s0]cc ==> D_s-  mu+ Neutrino ]CC'))"


_bs2dsmunu = CombineParticles("bs2dsmunu")
_bs2dsmunu.DecayDescriptor = "[B_s0 -> D_s- mu+]cc"
#_bs2dsmunu.DaughtersCuts = { "D_s+" : matchDs, "mu-" : matchmuons }
_bs2dsmunu.MotherCut = matchBs2Dsmunu
_bs2dsmunu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2Dsmunu = Selection( "SelBs2Dsmunu",
                        Algorithm = _bs2dsmunu,
                        RequiredSelections=[_mcds,_muons])  

SeqBs2Dsmunu = SelectionSequence('MCFilter',TopSelection = SelBs2Dsmunu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2Dsmunu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2Dsmunu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2Dsmunu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

