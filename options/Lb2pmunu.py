########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCMuons, StandardMCProtons
#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')
_protons = DataOnDemand(Location='Phys/StdMCProtons/Particles')


#
# MC matching
#


matchLb2pmunu   = "(mcMatch('[Lambda_b0 ==> p+ mu- Neutrino ]CC'))"
#matchprotons   = "(mcMatch('[p+]cc'))"
#matchmuons  = "(mcMatch('[mu-]cc'))"

_lb2pmunu = CombineParticles("lb2pmunu")
_lb2pmunu.DecayDescriptor = "[Lambda_b0 -> p+ mu-]cc"
#_lb2pmunu.DaughtersCuts = { "p+" : matchprotons, "mu-" : matchmuons }
_lb2pmunu.MotherCut = matchLb2pmunu
_lb2pmunu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelLb2pmunu = Selection( "SelLb2pmunu",
                        Algorithm = _lb2pmunu,
                        RequiredSelections=[_protons,_muons])  

SeqLb2pmunu = SelectionSequence('MCFilter',TopSelection = SelLb2pmunu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqLb2pmunu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqLb2pmunu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqLb2pmunu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

