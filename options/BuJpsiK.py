########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPhi, StandardMCJpsi, StandardMCKaons, StandardMCMuons

#Truth matched commonparticles: 
_jpsi = DataOnDemand(Location='Phys/StdMCJpsi2MuMu/Particles')
_kaon = DataOnDemand(Location='Phys/StdMCKaons/Particles')


#
# MC matching
#

matchBu2JpsiK  = "(mcMatch('[B+  ==> J/psi(1S)  K+]CC'))"


_bu2jpsik = CombineParticles("bu2jpsik")
_bu2jpsik.DecayDescriptor = "[B+ -> J/psi(1S)  K+]cc"
_bu2jpsik.MotherCut = matchBu2JpsiK
_bu2jpsik.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBu2JpsiK = Selection( "SelBu2JpsiK",
                        Algorithm = _bu2jpsik,
                        RequiredSelections=[_jpsi,_kaon])  

SeqBu2JpsiK = SelectionSequence('MCFilter',TopSelection = SelBu2JpsiK) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBu2JpsiK],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBu2JpsiK.sequence(), dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

