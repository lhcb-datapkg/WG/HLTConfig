########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKs, StandardMCMuons

#Truth matched commonparticles: 

_mcksll = DataOnDemand(Location='Phys/StdMCKsLL/Particles')
_mcksdd = DataOnDemand(Location='Phys/StdMCKsDD/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')

#
# MC matching
#

matchBd2Ksmumu   = "(mcMatch('[B0  ==>  KS0  mu+ mu-]CC'))"
#matchmuons   = "(mcMatch('[mu-]cc'))"
#matchks   = "(mcMatch('[KS0]cc'))"

_bd2ksmumu = CombineParticles("bd2ksmumu")
_bd2ksmumu.DecayDescriptor = "[B0 -> KS0  mu+ mu-]cc"
#_bd2ksmumu.DaughtersCuts = { "KS0" : matchks, "mu+" : matchmuons }
_bd2ksmumu.MotherCut = matchBd2Ksmumu
_bd2ksmumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]


SelBd2KsmumuDD = Selection( "SelBd2KsmumuDD",
                        Algorithm = _bd2ksmumu,
                        RequiredSelections=[_mcksdd,_muons])  

SeqBd2KsmumuDD = SelectionSequence('SeqBd2KsmumuDD',TopSelection = SelBd2KsmumuDD) 

SelBd2KsmumuLL = Selection( "SelBd2KsmumuLL",
                        Algorithm = _bd2ksmumu,
                        RequiredSelections=[_mcksll,_muons])  

SeqBd2KsmumuLL = SelectionSequence('SeqBd2KsmumuLL',TopSelection = SelBd2KsmumuLL) 

SeqBd2Ksmumu = MultiSelectionSequence('MCFilter',Sequences=[SeqBd2KsmumuLL,SeqBd2KsmumuDD])

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2Ksmumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2Ksmumu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2Ksmumu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

