########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCD0, StandardMCMuons

#Truth matched commonparticles: 

_mcd0 = DataOnDemand(Location='Phys/StdMCD02KPi/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#

#matchD0  = "(mcMatch('[D0]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"

#matchB2Dmunu  = "(mcMatch('[B+ ==> D~0 mu+ Nu {Nu}]CC'))"
matchB2Dmunu   = "(mcMatch('[B- ==>  D0  mu- Nu ]CC'))"

_b2d0munu = CombineParticles("b2d0munu")
_b2d0munu.DecayDescriptor = "[B- -> D0 mu-]cc"
#_b2d0munu.DaughtersCuts = { "D0" : matchD0, "mu+" : matchmuons }
_b2d0munu.MotherCut = matchB2Dmunu
_b2d0munu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelB2D0munu_D02KPi = Selection( "SelB2D0munu_D02KPi",
                        Algorithm = _b2d0munu,
                        RequiredSelections=[_mcd0,_muons])

SeqB2D0munu_D02KPi = SelectionSequence('MCFilter',TopSelection = SelB2D0munu_D02KPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqB2D0munu_D02KPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
#DaVinci().appendToMainSequence( SeqB2D0munu_D02KPi.sequence() )
#DaVinci().appendToMainSequence( dstWriter.sequence() )
DaVinci().UserAlgorithms = [SeqB2D0munu_D02KPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

