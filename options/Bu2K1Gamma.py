########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions, StandardMCPhotons, StandardMCKaons

# Build the K1

_pions = DataOnDemand(Location="Phys/StdMCPions/Particles")
_kaons = DataOnDemand(Location="Phys/StdMCKaons/Particles")
matchK1 = "(mcMatch('[K_1(1270)+ ==> K+ pi- pi+]CC'))"

## create the algorithm
StdMCK1 = CombineParticles("StdMCK1")
StdMCK1.DecayDescriptor = "[K_1(1270)+ -> K+ pi- pi+]cc"
StdMCK1.MotherCut = matchK1
#StdMCK1.DaughtersCuts = {"K+" : matchKaons, "pi+" : matchpions}
StdMCK1.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelStdMCK1 = Selection("SelStdMCK1",
                       Algorithm=StdMCK1,
                       RequiredSelections=[_pions, _kaons])

#Truth matched commonparticles:

_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')

#
# MC matching
#

#matchphi  = "(mcMatch('phi(1020)'))"
#matchgamma  = "(mcMatch('gamma'))"
matchBu2k1gamma  = "(mcMatch('[B+  ==> K_1(1270)+ gamma]CC'))"

_bu2k1gamma = CombineParticles("Bu2K1gamma")
_bu2k1gamma.DecayDescriptor = "[B+ -> K_1(1270)+ gamma]cc"
_bu2k1gamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV"}
_bu2k1gamma.MotherCut = matchBu2k1gamma
_bu2k1gamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBu2K1gamma = Selection( "SelBu2K1gamma",
                        Algorithm = _bu2k1gamma,
                        RequiredSelections=[SelStdMCK1,_gamma])

SeqBu2K1gamma = SelectionSequence('MCFilter',TopSelection = SelBu2K1gamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBu2K1gamma],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBu2K1gamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2KstGamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

