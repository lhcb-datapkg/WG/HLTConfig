########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKaons, StandardMCPions

#Truth matched commonparticles: 
#_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#

matchDs2PiPiPi = "(mcMatch('[D_s+  ==> pi- pi+ pi+]CC'))"
#matchKaons = "(mcMatch('[K+]cc'))"
#matchPions = "(mcMatch('[pi+]cc'))"

_ds2pipipi = CombineParticles("ds2pipipi")
_ds2pipipi.DecayDescriptor = "[D_s+ -> pi- pi+ pi+]cc"
#_ds2pipipi.DaughtersCuts = { "pi+" : matchPions, "K+" : matchKaons}
_ds2pipipi.MotherCut = matchDs2PiPiPi
_ds2pipipi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelDs2PiPiPi = Selection( "SelDs2PiPiPi",
                        Algorithm = _ds2pipipi,
                        RequiredSelections=[_pions])  

SeqDs2PiPiPi = SelectionSequence('MCFilter',TopSelection = SelDs2PiPiPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqDs2PiPiPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqDs2PiPiPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqDs2PiPiPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

