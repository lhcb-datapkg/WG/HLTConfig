########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCProtons, StandardMCPhotons, StandardMCKaons

# Build the Lambda*(1670)

_protons = DataOnDemand(Location="Phys/StdMCProtons/Particles")
_kaons   = DataOnDemand(Location="Phys/StdMCKaons/Particles")
matchLst = "(mcMatch('[Lambda(1670)0 ==> p+ K-]CC'))"

## create the algorithm
StdMCLst1670 = CombineParticles("StdMCLst1670")
StdMCLst1670.DecayDescriptor = "[Lambda(1670)0 -> p+ K-]cc"
StdMCLst1670.MotherCut = matchLst
StdMCLst1670.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelStdMCLst1670 = Selection("SelStdMCLst1670",
                            Algorithm=StdMCLst1670,
                            RequiredSelections=[_protons, _kaons])

#Truth matched commonparticles:

_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')

#
# MC matching
#

#matchphi  = "(mcMatch('phi(1020)'))"
#matchgamma  = "(mcMatch('gamma'))"
matchLb2Lst1670gamma  = "(mcMatch('[Lambda_b0  ==> Lambda(1670)0 gamma]CC'))"

_lb2lst1670gamma = CombineParticles("Lb2Lst1670Gamma")
_lb2lst1670gamma.DecayDescriptor = "[Lambda_b0 -> Lambda(1670)0 gamma]cc"
_lb2lst1670gamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV" }
_lb2lst1670gamma.MotherCut = matchLb2Lst1670gamma
_lb2lst1670gamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelLb2lst1670gamma = Selection( "SelLb2lst1670gamma",
                                Algorithm = _lb2lst1670gamma,
                                RequiredSelections=[SelStdMCLst1670,_gamma])

SeqLb2lst1670gamma = SelectionSequence('MCFilter',TopSelection = SelLb2lst1670gamma)

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqLb2lst1670gamma],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqLb2lst1670gamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2KstGamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

