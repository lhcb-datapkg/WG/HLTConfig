########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKstar, StandardMCElectrons

#Truth matched commonparticles: 
_mckst = DataOnDemand(Location='Phys/StdMCKstar/Particles')
_electrons = DataOnDemand(Location='Phys/StdMCElectrons/Particles')


#
# MC matching
#



matchBd2Kstee   = "(mcMatch('[B0  ==>  K*(892)0  e+ e-]CC'))"
#matchelectrons   = "(mcMatch('[e-]cc'))"
#matchkst   = "(mcMatch('[K*(892)0]cc'))"

#mothercut
mothercut = "ALL"

_bd2kstee = CombineParticles("bd2kstee")
_bd2kstee.DecayDescriptor = "[B0 -> K*(892)0  e+ e-]cc"
#_bd2kstee.DaughtersCuts = { "K*(892)0" : matchkst, "e+" : matchelectrons }
_bd2kstee.MotherCut = matchBd2Kstee
_bd2kstee.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2Kstee = Selection( "SelBd2Kstee",
                        Algorithm = _bd2kstee,
                        RequiredSelections=[_mckst,_electrons])  

SeqBd2Kstee = SelectionSequence('MCFilter',TopSelection = SelBd2Kstee) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2Kstee],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2Kstee.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2Kstee.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

