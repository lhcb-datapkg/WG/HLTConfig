########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions
#Truth matched commonparticles: 
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')

#
# MC matching
#


matchKs2pipi   = "(mcMatch('KS0 ==> pi- pi+'))"

_Ks2pipi = CombineParticles("Ks2pipi")
_Ks2pipi.DecayDescriptor = "KS0 -> pi- pi+"

_Ks2pipi.MotherCut = matchKs2pipi
_Ks2pipi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelKs2pipi = Selection( "SelKs2pipi",
                        Algorithm = _Ks2pipi,
                        RequiredSelections=[_pions])  

SeqKs2pipi = SelectionSequence('MCFilter',TopSelection = SelKs2pipi) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqKs2pipi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqKs2pipi.sequence(), dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

