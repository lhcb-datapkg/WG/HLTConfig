########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCLambdac, StandardMCPions
#Truth matched commonparticles: 
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')
_lambdacs = DataOnDemand(Location='Phys/StdMCLambdac/Particles')


#
# MC matching
#


matchLb2LcPi   = "(mcMatch('[Lambda_b0 ==> Lambda_c+ pi- ]CC'))"

_lb2lcpi = CombineParticles("lb2lcpi")
_lb2lcpi.DecayDescriptor = "[Lambda_b0 -> Lambda_c+ pi-]cc"
#_lb2lcpi.DaughtersCuts = { "p+" : matchprotons, "mu-" : matchmuons }
_lb2lcpi.MotherCut = matchLb2LcPi
_lb2lcpi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelLb2LcPi = Selection( "SelLb2LcPi",
                        Algorithm = _lb2lcpi,
                        RequiredSelections=[_pions,_lambdacs])  

SeqLb2LcPi = SelectionSequence('MCFilter',TopSelection = SelLb2LcPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqLb2LcPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqLb2LcPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqLb2LcPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

