########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPhi, StandardMCKaons

#Truth matched commonparticles: 
_phi = DataOnDemand(Location='Phys/StdMCPhi2KK/Particles')


#
# MC matching
#
#matchJpsi   = "(mcMatch('J/psi(1S)'))"
#matchphi  = "(mcMatch('phi(1020)'))"

matchBs2PhiPhi  = "(mcMatch('[B_s0  ==> phi(1020)  phi(1020)]CC'))"


_bs2phiphi = CombineParticles("bs2phiphi")
_bs2phiphi.DecayDescriptor = "[B_s0 -> phi(1020)  phi(1020)]cc"
#_bs2phiphi.DaughtersCuts = { "J/psi(1S)" : matchJpsi, "phi(1020)" : matchphi }
_bs2phiphi.MotherCut = matchBs2PhiPhi
_bs2phiphi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2PhiPhi = Selection( "SelBs2PhiPhi",
                        Algorithm = _bs2phiphi,
                        RequiredSelections=[_phi])  

SeqBs2PhiPhi = SelectionSequence('MCFilter',TopSelection = SelBs2PhiPhi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2PhiPhi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2PhiPhi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2PhiPhi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

