########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKs, StandardMCPions

#Truth matched commonparticles: 

_mcksll = DataOnDemand(Location='Phys/StdMCKsLL/Particles')
_mcksdd = DataOnDemand(Location='Phys/StdMCKsDD/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#


#matchKaons = "(mcMatch('[K+]cc'))"
#matchKshorts = "(mcMatch('[KS0]cc'))"
matchBd2KsPiPi = "(mcMatch('[B0  ==> KS0 pi+ pi-]CC'))"

#mothercut
mothercut = "ALL"

_bd2kspipi = CombineParticles("bd2kspipi")
_bd2kspipi.DecayDescriptor = "[B0 -> KS0 pi+ pi-]cc"
#_bd2kspipi.DaughtersCuts = { "KS0" : matchKshorts, "K+" : matchKaons}
_bd2kspipi.MotherCut = matchBd2KsPiPi
_bd2kspipi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2KsPiPiLL = Selection( "SelBd2KsPiPiLL",
                        Algorithm = _bd2kspipi,
                        RequiredSelections=[_pions,_mcksll])  

SeqBd2KsPiPiLL = SelectionSequence('SeqBd2KsPiPiLL',TopSelection = SelBd2KsPiPiLL) 

SelBd2KsPiPiDD = Selection( "SelBd2KsPiPiDD",
                        Algorithm = _bd2kspipi,
                        RequiredSelections=[_pions,_mcksdd])  


SeqBd2KsPiPiDD = SelectionSequence('SeqBd2KsPiPiDD',TopSelection = SelBd2KsPiPiDD) 

SeqBd2KsPiPi = MultiSelectionSequence('MCFilter',Sequences = [SeqBd2KsPiPiLL,SeqBd2KsPiPiDD]) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2KsPiPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2KsPiPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2KsPiPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

