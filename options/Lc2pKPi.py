########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions, StandardMCKaons, StandardMCProtons

#Truth matched commonparticles: 
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')
_protons = DataOnDemand(Location='Phys/StdMCProtons/Particles')


#
# MC matching
#


matchLcpKPi = "(mcMatch('[Lambda_c+ ==> p+ K- pi+]CC'))"
#matchprotons = "(mcMatch('[p+]cc'))"
#matchKaons = "(mcMatch('[K-]cc'))"
#matchPions = "(mcMatch('[pi+]cc'))"

_lc2pkpi = CombineParticles("lc2pkpi")
_lc2pkpi.DecayDescriptor = "[Lambda_c+ -> p+ K- pi+]cc"
#_lc2pkpi.DaughtersCuts = { "p+" : matchprotons, "K-" : matchKaons, "pi+" : matchPions }
_lc2pkpi.MotherCut = matchLcpKPi
_lc2pkpi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelLc2pKPi = Selection( "SelLc2pKPi",
                        Algorithm = _lc2pkpi,
                        RequiredSelections=[_kaons,_pions,_protons])  

SeqLc2pKPi = SelectionSequence('MCFilter',TopSelection = SelLc2pKPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqLc2pKPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqLc2pKPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqLc2pKPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

