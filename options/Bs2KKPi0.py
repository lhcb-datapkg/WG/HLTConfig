########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKaons, StandardMCPi0

#Truth matched commonparticles: 
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_respiz = DataOnDemand(Location='Phys/StdMCResolvedPi0/Particles')
_merpiz = DataOnDemand(Location='Phys/StdMCMergedPi0/Particles')

#
# MC matching
#

#matchKaons = "(mcMatch('[K+]cc'))"
#matchKshorts = "(mcMatch('[KS0]cc'))"
matchBs2KKPi0 = "(mcMatch('[B_s0  ==> K+ K- pi0]CC'))"


_bs2kkpi0 = CombineParticles("bs2kkpi0")
_bs2kkpi0.DecayDescriptor = "[B_s0 -> K+ K- pi0]cc"
_bs2kkpi0.MotherCut = matchBs2KKPi0
_bs2kkpi0.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]


SelBs2KKMergedPi0 = Selection( "SelBs2KKMergedPi0",
                        Algorithm = _bs2kkpi0,
                        RequiredSelections=[_kaons,_merpiz])  
SeqBs2KKMergedPi0 = SelectionSequence('SeqBs2KKMergedPi0',TopSelection = SelBs2KKMergedPi0) 

SelBs2KKResolvedPi0 = Selection( "SelBs2KKResolvedPi0",
                        Algorithm = _bs2kkpi0,
                        RequiredSelections=[_kaons,_respiz])  

SeqBs2KKResolvedPi0 = SelectionSequence('SeqBs2KKResolvedPi0',TopSelection = SelBs2KKResolvedPi0) 


SeqBs2KKPi0 = MultiSelectionSequence('MCFilter',Sequences = [SeqBs2KKResolvedPi0, SeqBs2KKMergedPi0]) 



#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2KKPi0],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2KKPi0.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2KKPi0.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

