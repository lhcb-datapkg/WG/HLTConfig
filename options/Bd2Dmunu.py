########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDplus, StandardMCMuons

#Truth matched commonparticles: 

#_mcd0 = DataOnDemand(Location='Phys/StdMCD02KPi/Particles')
_mcdplus = DataOnDemand(Location='Phys/StdMCDplus2KPiPi/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#


matchBd2Dmunu  = "(mcMatch('[[B0]cc ==>  D-  mu+ Neutrino ]CC'))"
#matchDplus   = "(mcMatch('[D+]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"


_bd2dmunu = CombineParticles("bd2dmunu")
_bd2dmunu.DecayDescriptor = "[B0 -> D- mu+]cc"
#_bd2dmunu.DaughtersCuts = { "D-" : matchDplus, "mu+" : matchmuons }
_bd2dmunu.MotherCut = matchBd2Dmunu
_bd2dmunu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2Dmunu = Selection( "SelBd2Dmunu",
                        Algorithm = _bd2dmunu,
                        RequiredSelections=[_mcdplus,_muons])  

SeqBd2Dmunu = SelectionSequence('MCFilter',TopSelection = SelBd2Dmunu) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2Dmunu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2Dmunu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2Dmunu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

