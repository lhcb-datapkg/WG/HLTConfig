########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKaons, StandardMCPions

#Truth matched commonparticles: 

_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#

#matchKaons = "(mcMatch('[K+]cc'))"
#matchKshorts = "(mcMatch('[KS0]cc'))"
matchB2KKPi = "(mcMatch('[B+  ==> K+ K- pi+]CC'))"


_b2kkpi = CombineParticles("b2kkpi")
_b2kkpi.DecayDescriptor = "[B+ -> K+ K- pi+]cc"
_b2kkpi.MotherCut = matchB2KKPi
_b2kkpi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelB2KKPi = Selection( "SelB2KKPi",
                        Algorithm = _b2kkpi,
                        RequiredSelections=[_kaons,_pions])  

SeqB2KKPi = SelectionSequence('MCFilter',TopSelection = SelB2KKPi) 


#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqB2KKPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqB2KKPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqB2KKPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

