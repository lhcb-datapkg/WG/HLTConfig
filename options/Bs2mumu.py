########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCDsplus, StandardMCMuons

#Truth matched commonparticles: 
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#

matchBs2mumu  = "(mcMatch('[[B_s0]cc ==> mu-  mu+ ]CC'))"


_bs2mumu = CombineParticles("bs2mumu")
_bs2mumu.DecayDescriptor = "B_s0 -> mu- mu+"
_bs2mumu.MotherCut = matchBs2mumu
_bs2mumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2mumu = Selection( "SelBs2mumu",
                        Algorithm = _bs2mumu,
                        RequiredSelections=[_muons])  

SeqBs2mumu = SelectionSequence('MCFilter',TopSelection = SelBs2mumu) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2mumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2mumu.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2mumu.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

