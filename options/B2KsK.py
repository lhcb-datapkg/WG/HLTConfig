########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKs, StandardMCKaons

#Truth matched commonparticles: 

_mcksll = DataOnDemand(Location='Phys/StdMCKsLL/Particles')
_mcksdd = DataOnDemand(Location='Phys/StdMCKsDD/Particles')
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')


#
# MC matching
#

#matchKaons = "(mcMatch('[K+]cc'))"
#matchKshorts = "(mcMatch('[KS0]cc'))"
matchB2KsK = "(mcMatch('[B+  ==> KS0 K+]CC'))"


_b2ksk = CombineParticles("b2ksk")
_b2ksk.DecayDescriptor = "[B+ -> KS0 K+]cc"
#_b2ksk.DaughtersCuts = { "KS0" : matchKshorts, "K+" : matchKaons}
_b2ksk.MotherCut = matchB2KsK
_b2ksk.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelB2KsKLL = Selection( "SelB2KsKLL",
                        Algorithm = _b2ksk,
                        RequiredSelections=[_kaons,_mcksll])  

SeqB2KsKLL = SelectionSequence('SeqB2KsKLL',TopSelection = SelB2KsKLL) 

SelB2KsKDD = Selection( "SelB2KsKDD",
                        Algorithm = _b2ksk,
                        RequiredSelections=[_kaons,_mcksdd])  

SeqB2KsKDD = SelectionSequence('SeqB2KsKDD',TopSelection = SelB2KsKDD) 

SeqB2KsK = MultiSelectionSequence('MCFilter',Sequences = [SeqB2KsKLL,SeqB2KsKDD]) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqB2KsK],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqB2KsK.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqB2KsK.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

