########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKaons, StandardMCPions

#Truth matched commonparticles: 
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#

matchD2KKPi = "(mcMatch('[D+  ==> K- K+ pi+]CC'))"
#matchKaons = "(mcMatch('[K+]cc'))"
#matchPions = "(mcMatch('[pi+]cc'))"

_d2kkpi = CombineParticles("d2kkpi")
_d2kkpi.DecayDescriptor = "[D+ -> K- K+ pi+]cc"
#_d2kkpi.DaughtersCuts = { "pi+" : matchPions, "K+" : matchKaons}
_d2kkpi.MotherCut = matchD2KKPi
_d2kkpi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelD2KKPi = Selection( "SelD2KKPi",
                        Algorithm = _d2kkpi,
                        RequiredSelections=[_kaons,_pions])  

SeqD2KKPi = SelectionSequence('MCFilter',TopSelection = SelD2KKPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqD2KKPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqD2KKPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqD2KKPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

