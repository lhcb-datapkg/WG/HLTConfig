########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, MultiSelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCKaons, StandardMCPions

#Truth matched commonparticles: 


_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#


matchBd2KPi = "(mcMatch('[B0  ==> K+ pi-]CC'))"


_bd2kpi = CombineParticles("bd2kpi")
_bd2kpi.DecayDescriptor = "[B0 -> K+ pi-]cc"
_bd2kpi.MotherCut = matchBd2KPi
_bd2kpi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBd2KPi = Selection( "SelBd2KPi",
                        Algorithm = _bd2kpi,
                        RequiredSelections=[_kaons,_pions])  

SeqBd2KPi = SelectionSequence('MCFilter',TopSelection = SelBd2KPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqBd2KPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBd2KPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBd2KPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

