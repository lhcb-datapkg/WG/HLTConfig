########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCLambda0, StandardMCPhotons, StandardMCKaons

#Truth matched commonparticles:

_l0 = DataOnDemand(Location='Phys/StdMCLambda02pPi/Particles')
_gamma = DataOnDemand(Location='Phys/StdMCPhotons/Particles')


#
# MC matching
#

matchLb2L0gamma  = "(mcMatch('[Lambda_b0  ==> Lambda0 gamma]CC'))"

_lb2L0gamma = CombineParticles("lb2L0gamma")
_lb2L0gamma.DecayDescriptor = "[Lambda_b0 -> Lambda0 gamma]cc"
_lb2L0gamma.DaughtersCuts = { "gamma" : "PT>1.5*GeV"}
_lb2L0gamma.ParticleCombiners = {'' : 'ParticleAdder'}
_lb2L0gamma.MotherCut = matchLb2L0gamma
_lb2L0gamma.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelLb2L0gamma = Selection( "SelLb2L0gamma",
                           Algorithm = _lb2L0gamma,
                           RequiredSelections=[_l0,_gamma])

SeqLb2L0gamma = SelectionSequence('MCFilter',TopSelection = SelLb2L0gamma)

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqLb2L0gamma],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqLb2L0gamma.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [Seqlb2L0gamma.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

