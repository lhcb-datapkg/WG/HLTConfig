########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCJpsi, StandardMCKaons, StandardMCPions, StandardMCMuons

#Truth matched commonparticles: 
_jpsi = DataOnDemand(Location='Phys/StdMCJpsi2MuMu/Particles')
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#

matchBs2JpsiKKPiPi   = "(mcMatch('[B_s0 ==> J/psi(1S) K+ K- pi+ pi-]CC'))"
#matchJpsi   = "(mcMatch('J/psi(1S)'))"
#matchpions  = "(mcMatch('[pi-]cc'))"
#matchkaons  = "(mcMatch('[K+]cc'))"

_bs2jpsikkpipi = CombineParticles("bs2jpsikkpipi")
_bs2jpsikkpipi.DecayDescriptor = "[B_s0 -> J/psi(1S)  K+ K- pi+ pi-]cc"
#_bs2jpsikkpipi.DaughtersCuts = { "J/psi(1S)" : matchJpsi, "K+" : matchkaons, "pi+" : matchpions}
_bs2jpsikkpipi.MotherCut = matchBs2JpsiKKPiPi
_bs2jpsikkpipi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelBs2JpsiKKpipi = Selection( "SelBs2JpsiKKpipi",
                        Algorithm = _bs2jpsikkpipi,
                        RequiredSelections=[_jpsi,_kaons,_pions])  

SeqBs2JpsiKKpipi = SelectionSequence('MCFilter',TopSelection = SelBs2JpsiKKpipi) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ SeqBs2JpsiKKpipi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqBs2JpsiKKpipi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqBs2JpsiKKpipi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

