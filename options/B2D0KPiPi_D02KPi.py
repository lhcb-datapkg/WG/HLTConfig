########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCD0, StandardMCKaons, StandardMCPions

#Truth matched commonparticles: 

_mcd0 = DataOnDemand(Location='Phys/StdMCD02KPi/Particles')
_kaons = DataOnDemand(Location='Phys/StdMCKaons/Particles')
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')


#
# MC matching
#

#matchD0   = "(mcMatch('[D0]cc'))"
#matchmuons  = "(mcMatch('[mu+]cc'))"
matchB2D0KPiPi  = "(mcMatch('[B+ ==> D~0 K+ pi- pi+]CC'))"


_d2d0kpipi = CombineParticles("d2d0kpipi")
_d2d0kpipi.DecayDescriptor = "[B- -> D0 K- pi+ pi-]cc"
#_d2d0kpipi.DaughtersCuts = { "D0" : matchD0, "mu+" : matchmuons }
_d2d0kpipi.MotherCut = matchB2D0KPiPi
_d2d0kpipi.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

SelB2D0KPiPi_D02KPi = Selection( "SelB2D0KPiPi_D02KPi",
                        Algorithm = _d2d0kpipi,
                        RequiredSelections=[_mcd0,_kaons, _pions])  

SeqB2D0KPiPi_D02KPi = SelectionSequence('MCFilter',TopSelection = SelB2D0KPiPi_D02KPi) 

#
# Write DST
#
dstExtension = "." + DaVinci().InputType.lower()
enablePacking = True
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [SeqB2D0KPiPi_D02KPi],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [SeqB2D0KPiPi_D02KPi.sequence(), dstWriter.sequence() ])
#DaVinci().UserAlgorithms = [SeqB2D0KPiPi_D02KPi.sequence(), dstWriter.sequence() ]

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

