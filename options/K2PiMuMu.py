########################################################################
from Gaudi.Configuration import *
from Configurables import DaVinci 
from Configurables import GaudiSequencer, CombineParticles, FilterDesktop
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand
from CommonMCParticles import StandardMCPions,StandardMCMuons
#Truth matched commonparticles: 
_pions = DataOnDemand(Location='Phys/StdMCPions/Particles')
_muons = DataOnDemand(Location='Phys/StdMCMuons/Particles')


#
# MC matching
#


matchk2pimumu   = "(mcMatch('[K+ ==> pi+ mu- mu+ ]CC'))"

_k2pimumu = CombineParticles("k2pimumu")
_k2pimumu.DecayDescriptor = "[K+ -> pi+ mu- mu+]cc"

_k2pimumu.MotherCut = matchk2pimumu
_k2pimumu.Preambulo = [
    "from LoKiPhysMC.decorators import *",
    "from PartProp.Nodes import CC" ]

Selk2pimumu = Selection( "Selk2pimumu",
                        Algorithm = _k2pimumu,
                        RequiredSelections=[_pions, _muons])  

Seqk2pimumu = SelectionSequence('MCFilter',TopSelection = Selk2pimumu) 

#
# Write DST
#
enablePacking = True
dstExtension = "." + DaVinci().InputType.lower()
from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements
                                      )
SelDSTWriterElements = { 'default' : stripDSTElements(pack=enablePacking) }
SelDSTWriterConf = { 'default' : stripDSTStreamConf(pack=enablePacking,fileExtension=dstExtension,selectiveRawEvent=False) }
dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = [ Seqk2pimumu],
                          )
#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().ProductionType = "Stripping"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                        # Number of events
DaVinci().appendToMainSequence( [Seqk2pimumu.sequence(), dstWriter.sequence() ])

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

